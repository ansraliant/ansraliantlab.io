---
layout: post
title:  "Next Steps"
---
So, now that I have Jekyll running the next steps would be to customize this Theme. It's more easy said than done, specially for someone who has been a exclusively a backend developer for the past 7years or so.
I've seen CSS and Javascript only in the most basic tutorials, then I knew backend was my stuff.

Let's write a draft of roadmap of the this I need to do.

1. Propertly learn CSS and SASS

The objective here is to make it minimal. Just CSS to make it look decent. Almost to none javascript if possible. Call me old minded, but I like my websites to be lightweight. After all, all we want is to read text. Maybe it's my backend background that has influenced me.

I will write about my journey through the CSS world from the perspective of a backend developer.

Will anyone read this? I don't know. And probably don't want to know. I want to have a record, something I can have to see what I have done, the things I've been through in my professional life. I had this blog idea on hold for many years, and it's time to change that, and maybe along the way it might help someone, so it's a win win situation.


