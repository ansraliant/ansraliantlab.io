---
layout: post
title:  "Initial Commit"
---

This is it. I've finally made my mind on creating a blog. The main reason is to have a history of things I find and do. Many times I have found myself saying "i have done this before, why haven't I wrote about this when I did do it?". The answer is lazy. I'm lazy by nature. I guess we programmers are lazy, that's why we automate stuff and delegate repetitive task to our beloved machines, so we can focus on the important stuff.

This blog like many others, as you might already have imaged by the domain, is a tech blog. I will write about my adventures and the things I do. Hopefully it will help me remember what I do and help me understand it better. They say that teaching certain topic helps understading it.
